@servers(['staging' => ['dev_itcc_devops@172.28.1.107'], 'prod' => ['prod_itcc_devops@192.168.80.217']])

@setup
    $dir = "/var/www/it-command/"
@endsetup

@story('deploy-fresh')
    site-down
    git
    migrate-seed
    site-up
@endstory

@story('deploy', ['on' => ['staging']])
    site-down
    git
    site-up
@endstory

@story('prod-deploy', ['on' => ['prod']])
    site-down
    git
    site-up
@endstory

@story('deploy-with-migrate', ['on' => ['staging']])
    site-down
    git
    migrate
    site-up
@endstory

@story('prod-deploy-with-migrate', ['on' => ['prod']])
    site-down
    git
    migrate
    site-up
@endstory

@story('prod-deploy-with-composer-update', ['on' => ['prod']])
    site-down
    git
    composer
    migrate
    site-up
@endstory

@story('deploy-with-composer-update', ['on' => ['staging']])
    site-down
    git
    composer
    migrate
    site-up
@endstory

@story('cleanup', ['on' => ['staging']])
    git 
    clean 
@endstory

@story('cleanstaging', ['on' => ['staging']])
    git 
    cleanstaging
@endstory

@story('rollback')
    rollback
    site-up
@endstory

@task('site-down')
    cd {{ $dir }}
    php artisan down 
@endtask

@task('git')
    cd {{ $dir }}
    git fetch
    git checkout {{ $branch }}
    git pull
@endtask

@task('migrate-seed')
    cd {{ $dir }}
    composer install 
    php artisan migrate 
    php artisan db:seed --class=SiteStatusNewIncidentStatusMatrixSeeder
@endtask

@task('seeder')
    cd {{ $dir }}
@endtask

@task('migrate')
    cd {{ $dir }}
    php artisan migrate 
@endtask

@task('clean')
    cd {{ $dir }}
    git rm .gitlab-ci.yml
    git rm .env.testing 
    git rm .env.dusk.local
    git add . 
    git commit -m 'remove files'
    git push
@endtask

@task('cleanstaging')
    cd {{ $dir }}
    git rm .gitlab-ci.yml
    git add . 
    git commit -m 'remove files'
    git push
@endtask

@task('composer')
    cd {{ $dir }}
    composer update
@endtask

@task('site-up')
    cd {{ $dir }}
    php artisan up
@endtask

@task('rollback')
    cd {{ $dir }}
    git fetch 
    git checkout {{ $previousbranch }}
@endtask
